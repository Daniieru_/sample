#include "CircleEntity.h"



CircleEntity::CircleEntity()
{
}

CircleEntity::CircleEntity(b2World & world, float radius, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x * MPP, pos.y * MPP);

	m_Body = world.CreateBody(&bDef);

	b2CircleShape bShape;
	bShape.m_p.Set(0, 0); bShape.m_radius = radius * MPP;

	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);
	m_Body->SetAngularDamping(AngDumping);

	m_Shape.setRadius(radius);
	m_Shape.setOrigin(sf::Vector2f(radius, radius));
	m_Shape.setFillColor(sf::Color::Transparent); m_Shape.setOutlineColor(sf::Color::Green); m_Shape.setOutlineThickness(-1.0f);
}


CircleEntity::~CircleEntity()
{
}

void CircleEntity::LoadTextureFromFile(std::string imgDir)
{
	if (!m_Texture->loadFromFile(imgDir))
		m_Texture->loadFromFile(ErrorImgDir);
	m_Shape.setTexture(m_Texture);
}

void CircleEntity::UpdateGraphics()
{
	sf::Vector2f pos(m_Body->GetPosition().x * PPM, m_Body->GetPosition().y * PPM);
	m_Shape.setPosition(pos);
	m_Shape.setRotation((m_Body->GetAngle() * 180) / b2_pi);
}

sf::CircleShape CircleEntity::GetShape()
{
	return m_Shape;
}

void CircleEntity::SetColours(sf::Color color, float thickness, bool fill)
{
	m_Shape.setOutlineColor(color);
	m_Shape.setOutlineThickness(thickness);
	(fill) ? m_Shape.setFillColor(color) : m_Shape.setFillColor(sf::Color::Transparent);
}
