#pragma once
#include "Entity.h"
class RectangleEntity :															//rectangle-shaped entity
	public Entity
{
public:
	b2Body *m_Body;																//body that represents entity in physics world

	RectangleEntity();
																				//rectangle shaped entity
	RectangleEntity(b2World &world, float halfSizeW, float halfSizeH, b2Vec2 pos, b2BodyType type, float friction = defFriction,
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);
	~RectangleEntity();

	void LoadTextureFromFile(std::string imgDir);
	void UpdateGraphics();
	sf::RectangleShape GetShape();
	void SetColours(sf::Color color = sf::Color::Green, float thickness = -1.0f, bool fill = false);

private:
	sf::Texture *m_Texture;														//graphics stuff 
	sf::RectangleShape m_Shape;													//graphics shape (to be textured)
};

