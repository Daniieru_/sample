#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>
#include <iostream>
#include <string>

#include "EntitiesManager.h"

#define PPM 15.0f
#define MPP 1/PPM

struct Body
{
	Body()
	{
		m_Size = sf::Vector2f(50.0f, 50.0f);
	}
	Body(sf::Vector2f size)
	{
		m_Size = size;
	}

	sf::Vector2f m_Size;

	b2Body *m_pBody;
};



int main(void)
{

	sf::RenderWindow window(sf::VideoMode(800, 600), "RandomProject");
	window.setFramerateLimit(60);

	b2Vec2 gravity(0.0f, 9.8f);										//setting gravity to 9.8N (in b2d scene objects will go up)
	b2World world(gravity);											//creating world and passing G to it

	b2BodyDef groundBodyDef;										//ground body definition	
	groundBodyDef.position.Set(400.0f * MPP, 550.0f * MPP);

	b2Body* groundBody = world.CreateBody(&groundBodyDef);			//ceating ground body by passing body definition to the world object

	b2PolygonShape groundBox;										//creating groundBox(groundPolygon)
	groundBox.SetAsBox(20000.0f / 2 * MPP, 100.0f / 2 * MPP);		//set groundPolygon to a BoxShape (800/20  (in meters) )

	groundBody->CreateFixture(&groundBox, 0.0f);					//creating shape fixture by passing the shape directly to the body, and setting the shape density [kg/m]


	sf::RectangleShape g_ground(sf::Vector2f(20000.0f, 100.0f));		//graphics for ground ------------------------------------
	g_ground.setOrigin(10000.0f, 50.0f);
	g_ground.setPosition(groundBody->GetPosition().x * PPM, groundBody->GetPosition().y * PPM);
	g_ground.setFillColor(sf::Color::Transparent);
	g_ground.setOutlineColor(sf::Color::White);
	g_ground.setOutlineThickness(1.0f);
	
	EntitiesManager *Manager = new EntitiesManager(world);
	
	float
		friction = 0.3f,
		restitution = 1.0f,
		dentisy = 0.7f;

	sf::Font font;

	if (!font.loadFromFile("font.ttf"))
	{
		return 0;
		std::cout << "FONT LOAD ERROR";
		system("PAUSE");
	}

	std::string settings_string = "F: " + std::to_string(friction) + "\nR: " + std::to_string(restitution) + "\nD: " + std::to_string(dentisy);

	sf::Text settingsTxt(sf::String(settings_string), font, 15);
	settingsTxt.setPosition(0, 0);
	


	//kinematic borders

	b2BodyDef borderDef;
	b2PolygonShape borderShape;
	b2FixtureDef borderFixtureDef;


	float speed = 0.0f;
	

	while (window.isOpen())
	{
		settings_string = "F: " + std::to_string(friction) + "\nR: " + std::to_string(restitution) + "\nD: " + std::to_string(dentisy);
		settingsTxt.setString(sf::String(settings_string));

		window.clear();

		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Left)
				{
				}

				// change this (if u want to destroy the objects on right mouse click)
				else if (event.mouseButton.button == sf::Mouse::Button::Right)
				//{
				//	sf::Vector2f mousePos(event.mouseButton.x, event.mouseButton.y);
				//	for (int i = 0; i < m_Shapes.size(); i++)
				//	{
				//		if (m_Shapes[i].getGlobalBounds().contains(mousePos))
				//		{
				//			m_Shapes.erase(m_Shapes.begin() + i);
				//			world.DestroyBody(rectBodies[i].m_pBody);
				//			rectBodies.erase(rectBodies.begin() + i);
				//			break;
				//		}
				//	}
				//}
				break;

			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Q)
				{
					sf::Vector2f mousePos(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					Manager->AddEntity(CircleEntity(world, 25.0f, b2Vec2(mousePos.x, mousePos.y), b2_dynamicBody));
				}
				else if (event.key.code == sf::Keyboard::W)
				{
					sf::Vector2f mousePos(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					Manager->AddEntity(RectangleEntity(world, 25.0f, 25.0f, b2Vec2(mousePos.x, mousePos.y), b2_dynamicBody));
				}

				break;

			default:
				break;
			}
		}

		Manager->PxWorldStep();
		Manager->UpdateGraphics();
		Manager->DrawAll(window);

		window.draw(g_ground);
		window.draw(settingsTxt);

		window.display();
	}


	return 0;
}







/*

===OOO========OOO=======OOO========OOO=======OOO========OOO=======OOO========OOO=======OOO===========. \n
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n																							 \
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n
v?V\?v?V?v?V\?v?V?v?V\?v?V?v?V\?v?V?v?V\?v?V?v?V\?v?V?v?/-------------/V\?v?V?v?V\?v?V?v?V\?v?V?v?V\?v?/|\n
?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/-------------/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/ |\n
\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?//-------------/\?/\?/\?/\?/\?/\?/\?/\?/\?/?/\?//  |\n
?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?/\?//-------------/\?/\?/\?/\?/\?/\?/\?/?/\?/?/\?/ /   |\n
-----------------�-------?/------?-------�-------�--�----------------------------------------------|    |\n
|?\ ?\ ?\ ?\ |__<|\_____/|______<|>_____<|>_____<|\/|>______|CocaCola| France2016 |________|_______|    |\n
|<<-<<-<<-<<-|  / \     / \     / \     / \     / \/ \                                 /\n                 															           /
																					  /\n
																					 /\n

*/

