#include "Entity.h"


Entity::Entity()
{
}

//	Constructor for a circle-shaped entity
Entity::Entity(b2World &world, float radius, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x*MPP, pos.y*MPP);

	m_Body = world.CreateBody(&bDef);

	b2CircleShape bShape;
	bShape.m_p = b2Vec2(0.0f, 0.0f); bShape.m_radius = radius;

	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);

	m_Shape.setSize(sf::Vector2f(2 * radius, 2 * radius));
	m_Shape.setOrigin(sf::Vector2f(radius, radius));
	m_Shape.setFillColor(sf::Color::Transparent); m_Shape.setOutlineColor(sf::Color::Green); m_Shape.setOutlineThickness(1.0f);

}

Entity::Entity(b2World & world, float halfSizeW, float halfSizeH, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x*MPP, pos.y*MPP);

	m_Body = world.CreateBody(&bDef);

	b2PolygonShape bShape;
	bShape.SetAsBox(halfSizeW, halfSizeH);

	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);

	m_Shape.setSize(sf::Vector2f(2 * halfSizeW, 2 * halfSizeH));
	m_Shape.setOrigin(sf::Vector2f(halfSizeW, halfSizeH));
	m_Shape.setFillColor(sf::Color::Transparent); m_Shape.setOutlineColor(sf::Color::Green); m_Shape.setOutlineThickness(1.0f);

}

Entity::Entity(b2World & world, b2Vec2 * vertices, int count, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	b2PolygonShape bShape;
	bShape.Set(vertices, count);

	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x*MPP, pos.y*MPP);

	m_Body = world.CreateBody(&bDef);



	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);

	//sf::VertexArray vArray(sf::Lines, bShape.GetVertexCount());				//*** TO DO				

	int ptCount = bShape.GetVertexCount();

	m_ConvexShape.setPointCount(ptCount);

	int currPt = 0;

	for (int i = ptCount - 1; i >= 0; i--)
	{
		/*vArray[i].position = sf::Vector2f(bShape.GetVertex(i).x, bShape.GetVertex(i).y);
		vArray[i].color = sf::Color::Green;*/

		m_ConvexShape.setPoint(currPt, sf::Vector2f(bShape.GetVertex(i).x, bShape.GetVertex(i).y));
		currPt++;
	}
//	m_ConvexShape.setFillColor = sf::Color::Transparent; m_ConvexShape.setOutlineColor = sf::Color::Green;
	//m_ConvexShape.setOutlineThickness = 1.0f;
}


Entity::~Entity()
{
}

void Entity::LoadTextureFromFile(std::string imgDir)
{
	if (!m_Texture->loadFromFile(imgDir))
		m_Texture->loadFromFile(ErrorImgDir);
	m_Shape.setTexture(m_Texture);
}
